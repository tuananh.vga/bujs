/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

/**
 * A brief explanation for "project.json":
 * Here is the content of project.json file, this is the global configuration for your game, you can modify it to customize some behavior.
 * The detail of each field is under it.
 {
    "project_type": "javascript",
    // "project_type" indicate the program language of your project, you can ignore this field

    "debugMode"     : 1,
    // "debugMode" possible values :
    //      0 - No message will be printed.
    //      1 - cc.error, cc.assert, cc.warn, cc.log will print in console.
    //      2 - cc.error, cc.assert, cc.warn will print in console.
    //      3 - cc.error, cc.assert will print in console.
    //      4 - cc.error, cc.assert, cc.warn, cc.log will print on canvas, available only on web.
    //      5 - cc.error, cc.assert, cc.warn will print on canvas, available only on web.
    //      6 - cc.error, cc.assert will print on canvas, available only on web.

    "showFPS"       : true,
    // Left bottom corner fps information will show when "showFPS" equals true, otherwise it will be hide.

    "frameRate"     : 60,
    // "frameRate" set the wanted frame rate for your game, but the real fps depends on your game implementation and the running environment.

    "noCache"       : false,
    // "noCache" set whether your resources will be loaded with a timestamp suffix in the url.
    // In this way, your resources will be force updated even if the browser holds a cache of it.
    // It's very useful for mobile browser debugging.

    "id"            : "gameCanvas",
    // "gameCanvas" sets the id of your canvas element on the web page, it's useful only on web.

    "renderMode"    : 0,
    // "renderMode" sets the renderer type, only useful on web :
    //      0 - Automatically chosen by engine
    //      1 - Forced to use canvas renderer
    //      2 - Forced to use WebGL renderer, but this will be ignored on mobile browsers

    "engineDir"     : "frameworks/cocos2d-html5/",
    // In debug mode, if you use the whole engine to develop your game, you should specify its relative path with "engineDir",
    // but if you are using a single engine file, you can ignore it.

    "modules"       : ["cocos2d"],
    // "modules" defines which modules you will need in your game, it's useful only on web,
    // using this can greatly reduce your game's resource size, and the cocos console tool can package your game with only the modules you set.
    // For details about modules definitions, you can refer to "../../frameworks/cocos2d-html5/modulesConfig.json".

    "jsList"        : [
    ]
    // "jsList" sets the list of js files in your game.
 }
 *
 */

let GLOBAL_CONSTS = {
    chanceDist_: 80,
    baseResultLine_: 130,
    arrowAnimationTime_: 135,
    glowAnimationTime_ : 1000,
    glowAnimationOpacity_ : 80,
    laneYStart_: this.height - 350,
    lane1Yofs_: 3,
    lane2Yofs_: 3 + 64,        // Renderer_.spritePos_.lane1Yofs + 64,
    lane3Yofs_: 3 + 64 + 64,     // Renderer_.spritePos_.lane2Yofs + 64,
    lane2Xofs_: 5,
    laneWidth_: 256,
    tableWidth_: 123,
    tableWidthTrans_: 3,
    arrowLaneOfs_: 1,
    spaceMarginBottom_: 80,
    beatupLetterDist_: 46,
    dnxpLogoMargin_: 20,
    textHeight_: 20,
    textMarginTop_: 64,
    numNotes_: 14,
    playerListUp_: 40,
    playerListName_: 200,
    playerListScore_: 60,
    playerListYofs_: 80,
    scoreTableXofs_: (this.width - 600) / 2,
    fontSize_: 11,
    helpYofs_: 150
};

let getArrowPosition = function(key){
    let yOffset = GLOBAL_CONSTS.lane1Yofs_;
    if (key === 4 || key === 6)
        yOffset = GLOBAL_CONSTS.lane2Yofs_;
    else if (key === 7 || key === 9)
        yOffset = GLOBAL_CONSTS.lane3Yofs_;

    let yPos = yOffset + GLOBAL_CONSTS.laneYStart_ + 92;

    let xPos = GLOBAL_CONSTS.tableWidth_ - GLOBAL_CONSTS.tableWidthTrans_ +
        GLOBAL_CONSTS.laneWidth_ - GLOBAL_CONSTS.chanceDist_ + GLOBAL_CONSTS.arrowLaneOfs_ + 32;
    if (key === 4 || key === 6)
        xPos += 5;
    if (key % 3 === 0)
        xPos = GLOBAL_CONSTS.canvasWidth_ - xPos;

    return cc.p(xPos,yPos);
};

cc.game.onStart = function(){
    var sys = cc.sys;
    if(!sys.isNative && document.getElementById("cocosLoading")) //If referenced loading.js, please remove it
        document.body.removeChild(document.getElementById("cocosLoading"));

    // Pass true to enable retina display, on Android disabled by default to improve performance
    cc.view.enableRetina(sys.os === sys.OS_IOS ? true : false);

    // Disable auto full screen on baidu and wechat, you might also want to eliminate sys.BROWSER_TYPE_MOBILE_QQ
    if (sys.isMobile && 
        sys.browserType !== sys.BROWSER_TYPE_BAIDU &&
        sys.browserType !== sys.BROWSER_TYPE_WECHAT) {
        cc.view.enableAutoFullScreen(true);
    }

    // Adjust viewport meta
    cc.view.adjustViewPort(true);

    // Uncomment the following line to set a fixed orientation for your game
    // cc.view.setOrientation(cc.ORIENTATION_PORTRAIT);

    // Setup the resolution policy and design resolution size
    let designSize = cc.size(980,400);
    cc.view.setDesignResolutionSize(designSize.width, designSize.height, cc.ResolutionPolicy.SHOW_ALL);
    GLOBAL_CONSTS.laneYStart_ = designSize.height - 350;
    GLOBAL_CONSTS.scoreTableXofs_ = (designSize - 600) / 2;
    GLOBAL_CONSTS.canvasWidth_ = designSize.width;
    GLOBAL_CONSTS.canvasHeight_ = designSize.height;

    // The game will be resized when browser size change
    cc.view.resizeWithBrowserSize(true);

    //load resources
    cc.LoaderScene.preload(g_resources, function () {
        cc.spriteFrameCache.addSpriteFrames(res.bu_plist,res.bu_png);
        let request = cc.loader.getXMLHttpRequest();
        request.open("GET","/notes/list.json",true);
        request.onreadystatechange = function (){
            if (this.readyState == 4){
                if (request.status == 200){
                    let songs = JSON.parse(request.responseText);
                    cc.director.runScene(new SongSelectScene(songs));
                } else{
                    cc.LoaderScene._label.setText("Cannot get song list");
                }
            }
        }
        request.send();
        //cc.director.runScene(new BUScene());
    }, this);
};
cc.game.run();