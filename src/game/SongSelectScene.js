let SongSelectScene = cc.Scene.extend({
    ctor: function (songs) {
        this._super();
        console.log(songs);

        let scrollView = new ccui.ListView();
        scrollView.setContentSize(cc.size(this.width, this.height));
        scrollView.setAnchorPoint(cc.p(0.5, 0.5));
        scrollView.setDirection(ccui.ScrollView.DIR_VERTICAL);
        scrollView.setTouchEnabled(true);
        scrollView.setScrollBarEnabled(false);
        scrollView.setPosition(this.width / 2, this.height / 2);
        scrollView.setGravity(ccui.ListView.GRAVITY_CENTER_HORIZONTAL);
        this.addChild(scrollView);

        for (let name in songs) {
            let song = songs[name];
            song.propName = name;
            let label = new cc.LabelTTF(song["name"] + " - " + song["slkauthor"], "Arial", 16);
            label.setAnchorPoint(0, 0);

            let play = new ccui.Button("a68.png", "", "", ccui.Widget.PLIST_TEXTURE);
            play.addClickEventListener(function () {
                console.log(song);
                let scene = new BUScene();
                cc.director.runScene(scene);
                scene.setSong(song);
            });
            play.setScale(0.25);
            play.setPosition(label.width + 10, label.height / 2);

            let container = new ccui.Widget();
            container.setContentSize(label.width + 30, label.height + 15);
            container.addChild(label);
            container.addChild(play);
            scrollView.pushBackCustomItem(container);
        }
    }
});