let KeyResultFrame = cc.Node.extend({
    ctor: function () {
        this._super();
        this.consts_ = GLOBAL_CONSTS;

        this._lastKeyResult = -1;
        this._lastKeyTime = -1;

        this._initKeyResultSprites();
        this._initExplodeSprites();

        this.scheduleUpdate();
    },

    update: function (dt) {
        let currentTime = Date.now();

        // update explode children's opacity
        this._updateExplodeOpacity(this._lane1, this._lane4, this._lane7,
            this._lane3, this._lane6, this._lane9, this._laneSpace);

        if (currentTime - this._lastKeyTime > 300) {
            if (0 <= this._lastKeyResult && this._lastKeyResult < this._keyResultSprites.length) {
                this._keyResultSprites[this._lastKeyResult].setVisible(false);
            }
        }
    },

    _initKeyResultSprites: function () {
        let frames = ["#perfect.png", "#great.png", "#cool.png", "#bad.png", "#miss.png"];
        this._keyResultSprites = [];
        for (let i = 0; i < frames.length; i++) {
            let sprite = new cc.Sprite(frames[i]);
            sprite.setPosition(this.consts_.canvasWidth_ / 2,
                this.consts_.canvasHeight_ - this.consts_.baseResultLine_ + 50);
            sprite.setVisible(false);
            this.addChild(sprite);
            this._keyResultSprites.push(sprite);
        }
    },

    _initExplodeSprites: function () {
        let explodeNode = new cc.Node();
        this.addChild(explodeNode);

        let lane1 = this._createExplodeSprite(1);
        explodeNode.addChild(lane1);
        this._lane1 = lane1;

        let lane4 = this._createExplodeSprite(4);
        explodeNode.addChild(lane4);
        this._lane4 = lane4;

        let lane7 = this._createExplodeSprite(7);
        explodeNode.addChild(lane7);
        this._lane7 = lane7;

        let lane3 = this._createExplodeSprite(3);
        explodeNode.addChild(lane3);
        this._lane3 = lane3;

        let lane6 = this._createExplodeSprite(6);
        explodeNode.addChild(lane6);
        this._lane6 = lane6;

        let lane9 = this._createExplodeSprite(9);
        explodeNode.addChild(lane9);
        this._lane9 = lane9;

        let laneSpace = this._createExplodeSprite(5);
        explodeNode.addChild(laneSpace);
        this._laneSpace = laneSpace;
    },

    _createExplodeSprite: function (key) {
        let result = new cc.Node();
        result.setVisible(false);

        if (key === 5) {
            // space
            let spaceExplode = new cc.Sprite("#space_frame_space_explode.png");
            spaceExplode.setPosition(this.consts_.canvasWidth_ / 2, this.consts_.spaceMarginBottom_);
            result.addChild(spaceExplode);
            result.explode = spaceExplode;
            return result;
        }

        // arrow
        let explode = new cc.Sprite("#arrow_explode.png");
        explode.setPosition(getArrowPosition(key));

        result.addChild(explode);
        result.explode = explode;
        return result;
    },

    _showKeyExplode: function () {
        NoteFrame.prototype.showKeyInput.apply(this, arguments);
    },

    _updateExplodeOpacity: function () {
        for (let i = 0; i < arguments.length; i++) {
            let node = arguments[i];

            if (!node.isVisible()) {
                continue;
            }

            if (node.explode) {
                node.explode.opacity = node.opacity;
            }
        }
    },

    setKeyResult: function (result, key) {
        if (result >= 0 && result < this._keyResultSprites.length) {
            this._lastKeyResult = result;
            this._lastKeyTime = Date.now();
            for (let i = 0; i < this._keyResultSprites.length; i++) {
                this._keyResultSprites[i].setVisible(i == result);
                this._keyResultSprites[i].stopAllActions();
                if (i == result) {
                    let node = this._keyResultSprites[i];
                    node.setScale(14 / 9);
                    let action = new cc.ScaleTo(50 / 1000, 1);
                    node.runAction(action);
                }
            }
        }

        // no miss
        if (0 <= result && result < 4) {
            this._showKeyExplode(key);
        }
    },

    onExit: function () {
        cc.Node.prototype.onExit.apply(this, arguments);
        for (let i = 0; i < this._keyResultSprites; i++) {
            this._keyResultSprites[i].stopAllActions();
        }
    }
})