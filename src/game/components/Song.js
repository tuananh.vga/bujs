let Song = cc.Class.extend({
    ctor: function (songInfo) {
        let thiz = this;

        // copy props
        for (let prop in songInfo) {
            this[prop] = songInfo[prop];
        }

        // load aac
        let aacUrl = "/music/" + this["ogg"]
        if (cc.loader.cache.hasOwnProperty(aacUrl) == false) {
            cc.loader.load(aacUrl, function () {
                thiz._aacDone = true;
                thiz.onResourceLoaded();
            });
        }

        // load notes
        let request = cc.loader.getXMLHttpRequest();
        request.open("GET", "/notes/" + this["propName"] + ".json", true);
        request.onreadystatechange = function () {
            if (this.readyState == 4) {
                if (request.status == 200) {
                    let notes = JSON.parse(request.responseText);
                    thiz.notes = notes;
                    thiz._notesDone = true;
                    thiz.onResourceLoaded();
                } else {
                    console.log("Cannot get notes of song " + thiz.name);
                }
            }
        }
        request.send();
    },

    onResourceLoaded: function () {
        if (this._aacDone && this._notesDone) {
            if (this._readyFunc && typeof (this._readyFunc) === "function") {
                this._readyFunc(this);
            }
        }
    },

    setOnReadyFunction: function (cb) {
        this._readyFunc = cb;
        if (this._notesDone && this._aacDone) {
            cb(this);
        }
    }
});