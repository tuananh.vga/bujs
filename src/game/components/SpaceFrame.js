let SpaceLetter = cc.Node.extend({
    ctor: function (letter, index){
        cc.Node.prototype.ctor.call(this);

        let glowYellow = new cc.Sprite("#space_frame_letter_glow_yellow.png");
        SpaceFrame.prototype._applyGlowAnimation(glowYellow, index * GLOBAL_CONSTS.glowAnimationTime_ / 6);
        glowYellow.setVisible(false);
        this.addChild(glowYellow);
        this.glowYellow = glowYellow;

        let glowBlue = new cc.Sprite("#space_frame_letter_glow_blue.png");
        SpaceFrame.prototype._applyGlowAnimation(glowBlue, index * GLOBAL_CONSTS.glowAnimationTime_ / 6);
        this.addChild(glowBlue);
        this.glowBlue = glowBlue;

        let sprite = new cc.Sprite("#space_frame_letter_" + letter + ".png");
        this.letter = sprite;
        this.addChild(sprite);
    },

    /**
     * Sets the up state of letter <br/>
     * @function
     * @param {Number} state 0 = normal, 1 = yellow up, 2 = blue up
     */
    setUpState: function(state){
        // up blue
        if (state === 2){
            this.letter.setVisible(true);
            this.glowBlue.setVisible(true);
            this.glowYellow.setVisible(false);
        // up yellow
        } else if (state === 1){
            this.letter.setVisible(true);
            this.glowBlue.setVisible(false);
            this.glowYellow.setVisible(true);
        } else {
            this.letter.setVisible(false);
            this.glowYellow.setVisible(false);
            this.glowBlue.setVisible(false);
        }
    }
})

let SpaceFrame = cc.Node.extend({
    ctor: function(){
        this._super();
        this.consts_ = GLOBAL_CONSTS;

        this._initSpaceFrames();
        this.setCombo(0);
    },

    _initSpaceFrames: function (){
        let frame = new cc.Sprite("#space_frame.png");

        let glowBlue = new cc.Sprite("#space_frame_glow_blue.png");
        this._applyGlowAnimation(glowBlue);
        // glowBlue.setVisible(false);
        this.addChild(glowBlue);
        this.glowBlue = glowBlue;

        let glowYellow = new cc.Sprite("#space_frame_glow_yellow.png");
        glowYellow.setVisible(false);
        this._applyGlowAnimation(glowYellow);
        this.addChild(glowYellow);
        this.glowYellow = glowYellow;

        this.addChild(frame);

        // letters
        let letters = ["b","e","a","t","u","p"];
        let letterSprites = [];
        for (let i = 0;i<letters.length; i++){
            let sprite = new SpaceLetter(letters[i], i);
            sprite.setPosition(- this.consts_.beatupLetterDist_ / 2 * (5-i*2) + 1,0);
            letterSprites.push(sprite);
            this.addChild(sprite);
        }
        this.letterSprites = letterSprites;
    },

    _applyGlowAnimation: function(node,offset){
        let consts_ = this.consts_ || GLOBAL_CONSTS;
        if (typeof (offset) !== "number"){
            offset = Math.random() * consts_.glowAnimationTime_
        }
        setTimeout(function (){
            let fade1 = new cc.FadeTo(consts_.glowAnimationTime_/2/1000,consts_.glowAnimationOpacity_);
            let fade2 = new cc.FadeTo(consts_.glowAnimationTime_/2/1000,255);
            let sequence = new cc.RepeatForever(new cc.Sequence(fade1,fade2));
            node.stopAllActions();
            node.runAction(sequence);
        },offset);
    },

    /**
     * Sets the current combo <br/>
     * The default value is true
     * @function
     * @param {Number} combo current combo value
     */
    setCombo : function (combo){
        // update glow frames
        this.glowYellow.setVisible(combo >= 100 && combo < 400);
        this.glowBlue.setVisible(combo >= 400);

        // decide up state
        let numBlue = 0;
        let numYellow = 0;
        if (combo >= 400){
            // all blue
            numBlue = 6;
        }else if(combo >= 100){
            // mix blue + yellow
            numBlue = Math.floor((combo - 100) / 50);
            numYellow = 6;
        }else {
            // some yellow
            if (combo >= 80) numYellow = 5;
            else if (combo >= 60) numYellow = 4;
            else if (combo >= 40) numYellow = 3;
            else if (combo >= 20) numYellow = 2;
            else if (combo >= 10) numYellow = 1;
        }

        // update up state to letters
        for (let i = 0;i<this.letterSprites.length;i++){
            if (i < numBlue){
                this.letterSprites[i].setUpState(2); // blue
            } else if (i < numYellow){
                this.letterSprites[i].setUpState(1); // yellow
            } else{
                this.letterSprites[i].setUpState(0); // off
            }
        }
    }
});