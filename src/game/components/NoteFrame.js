let NoteFrame = cc.Node.extend({
    ctor: function () {
        this._super();

        this.consts_ = GLOBAL_CONSTS;

        this._initKeyVisualNode();
        this._initNotesPool();
        this._lastProcessNote = -1;
        this._notes = [];
        this._currentTime = 0;
    },

    _initKeyVisualNode: function () {
        let keyVisualNode = new cc.Node();
        this.addChild(keyVisualNode);

        let lane1 = this._createVisualNode(1);
        keyVisualNode.addChild(lane1);
        this._lane1 = lane1;

        let lane4 = this._createVisualNode(4);
        keyVisualNode.addChild(lane4);
        this._lane4 = lane4;

        let lane7 = this._createVisualNode(7);
        keyVisualNode.addChild(lane7);
        this._lane7 = lane7;

        let lane3 = this._createVisualNode(3);
        keyVisualNode.addChild(lane3);
        this._lane3 = lane3;

        let lane6 = this._createVisualNode(6);
        keyVisualNode.addChild(lane6);
        this._lane6 = lane6;

        let lane9 = this._createVisualNode(9);
        keyVisualNode.addChild(lane9);
        this._lane9 = lane9;

        let laneSpace = this._createSpaceVisualNode();
        keyVisualNode.addChild(laneSpace);
        this._laneSpace = laneSpace;
    },

    _createVisualNode: function (key) {
        let isLeftLane = !(key % 3 === 0);

        // lane down sprite
        let xPos = this.consts_.tableWidth_ - this.consts_.tableWidthTrans_ - this.consts_.chanceDist_ -
            this.consts_.arrowLaneOfs_;
        if (!isLeftLane)
            xPos = this.consts_.canvasWidth_ - (this.consts_.tableWidth_ - this.consts_.chanceDist_ +
                this.consts_.laneWidth_ - this.consts_.arrowLaneOfs_ + 3 + 64);	// 3 is a little weird here.
        let yOffset = this.consts_.lane1Yofs_;
        if (key === 4 || key === 6)
            yOffset = this.consts_.lane2Yofs_;
        else if (key === 7 || key === 9)
            yOffset = this.consts_.lane3Yofs_;

        let yPos = yOffset + this.consts_.laneYStart_ + 60;
        let laneDown = new cc.Sprite("#lane_" + key + ".png");
        laneDown.setAnchorPoint(cc.p(0, 0));
        laneDown.setPosition(xPos, yPos);

        // beat down sprite
        let beatDown = new cc.Sprite("#beatdown_" + key + ".png");
        // beatDown.setAnchorPoint(cc.p(0, 0));
        //
        // xPos = (this.consts_.tableWidth_ - this.consts_.tableWidthTrans_ +
        //     this.consts_.laneWidth_ - this.consts_.chanceDist_ + this.consts_.arrowLaneOfs_);
        // if (key === 4 || key === 6)
        //     xPos += 5;
        // if (key % 3 === 0)
        //     xPos = this.consts_.canvasWidth_ - xPos - beatDown.width;
        beatDown.setPosition(getArrowPosition(key));

        let result = new cc.Node();
        result.addChild(laneDown);
        result.addChild(beatDown);
        result.laneDown = laneDown;
        result.beatDown = beatDown;
        result.setVisible(false);
        return result;
    },

    _createSpaceVisualNode: function(){
        let spaceDown = new cc.Sprite("#space_frame_explode.png");
        spaceDown.setPosition(this.consts_.canvasWidth_/ 2,this.consts_.spaceMarginBottom_);

        let result = new cc.Node();
        result.addChild(spaceDown);
        result.spaceDown = spaceDown;
        result.setVisible(false);
        return result;
    },

    _initNotesPool: function () {
        this.MAX_NOTES = 50;
        this._noteSprites = [];
        for (let i = 0; i < this.MAX_NOTES; i++) {
            let note = new cc.Sprite("#a11.png");
            note.setAnchorPoint(cc.p(0, 0));
            note.setVisible(false);
            this.addChild(note);
            this._noteSprites.push(note);
        }
    },

    onEnter: function () {
        this._super();
        this.scheduleUpdate();
    },

    onExit: function () {
        this._super();
        this.unscheduleUpdate();
        this._lane1.stopAllActions();
        this._lane4.stopAllActions();
        this._lane7.stopAllActions();
        this._lane3.stopAllActions();
        this._lane6.stopAllActions();
        this._lane9.stopAllActions();
    },

    update: function (dt) {
        if (!this._started)
            return;
        this._currentTime += (dt * 1000);

        // update visual children's opacity
        this._updateVisualOpacity(this._lane1,this._lane4,this._lane7,
            this._lane3,this._lane6,this._lane9,this._laneSpace);

        let maxArrowTime = this._currentTime + this._maxArrowAvailTime;
        let maxSpaceTime = this._currentTime + this._maxSpaceAvailTime;

        // process existing note
        for (let i = 0; i < this._lastProcessNote + 1; i++) {
            let note = this._notes[i];
            if (note._pressed)
                continue;
            if (note.n !== 5) {
                if (this._noteSprites[i % this.MAX_NOTES].isVisible() === false)
                    continue;
                this._updateArrowNote(this._noteSprites[i % this.MAX_NOTES], note);
            } else {
                this._updateSpaceNote(note);
            }
        }

        // process new note
        for (let i = this._lastProcessNote + 1; i < this._notes.length; i++) {
            let note = this._notes[i];
            if ((note.n !== 5 && note.t > maxArrowTime) ||
                note.n === 5 && note.t > maxSpaceTime)
                break;

            if (note.n !== 5) {
                let sprite = this._noteSprites[i % this.MAX_NOTES];
                this._updateArrowNote(sprite, note);
            } else {
                // a space
                let lSpace = new cc.Sprite("#space_frame_cursor.png");
                let rSpace = new cc.Sprite("#space_frame_cursor.png");
                this.addChild(lSpace);
                this.addChild(rSpace);
                note._lSpace = lSpace;
                note._rSpace = rSpace;
                this._updateSpaceNote(note);
            }
            this._lastProcessNote = i;
        }
    },

    start: function (notes, tickTime) {
        this._notes = notes;
        this._tickTime = tickTime;
        this._currentTime = 0;
        this._firstAvailNote = 0;
        this._maxArrowAvailTime = tickTime * (this.consts_.numNotes_ + 1);
        this._maxSpaceAvailTime = tickTime * 8;
        this._started = true;
    },

    /**
     * Remove note index from displaying
     * @param index
     */
    removeNote: function (index) {
        if (0 <= index && index < this._notes.length) {
            if (this._notes[index].n !== 5)
                this._noteSprites[index % this.MAX_NOTES].setVisible(false);
            else {
                this._notes[index]._lSpace && this._notes[index]._lSpace.removeFromParent();
                this._notes[index]._lSpace = undefined;
                this._notes[index]._rSpace && this._notes[index]._rSpace.removeFromParent();
                this._notes[index]._rSpace = undefined;
            }
        }
    },

    showKeyInput: function(keyCode){
        let node = this._lane1;
        switch (keyCode){
            case 1:
                break;
            case 4:
                node = this._lane4;
                break;
            case 7:
                node = this._lane7;
                break;
            case 3:
                node = this._lane3;
                break;
            case 6:
                node = this._lane6;
                break;
            case 9:
                node = this._lane9;
                break;
            case 5:
                node = this._laneSpace;
                break;
            default:
                return;
        }

        node.stopAllActions();
        node.setVisible(true);
        node.setOpacity(255);
        let fadeAction = new cc.FadeTo(0.135,0);
        let hideAction = new cc.CallFunc(function (){
            node.setVisible(false);
        },node);
        node.runAction(new cc.Sequence(fadeAction,hideAction));
    },

    _updateVisualOpacity: function(){
        for (let i = 0;i<arguments.length;i++){
            let node = arguments[i];

            if (!node.isVisible()){
                continue;
            }

            if (node.laneDown && node.beatDown){
                node.laneDown.opacity = node.beatDown.opacity = node.opacity;
            }

            if (node.spaceDown){
                node.spaceDown.opacity = node.opacity;
            }
        }
    },

    _updateArrowNote: function (sprite, note) {
        let spriteFrameName = "a" + note.n;
        let timeDiff = Math.abs(this._currentTime - note.t);
        let imageIndex = Math.round(timeDiff / this._tickTime) % 4;

        let yPos = this.consts_.lane1Yofs_;
        if (note.n === 4 || note.n === 6) yPos = this.consts_.lane2Yofs_ - 1;
        else if (note.n === 7 || note.n === 9) yPos = this.consts_.lane3Yofs_;
        yPos += (this.consts_.laneYStart_ + 61);

        let xPos = (this.consts_.tableWidth_ - this.consts_.tableWidthTrans_ +
            this.consts_.laneWidth_ - this.consts_.chanceDist_ + this.consts_.arrowLaneOfs_) -
            (note.t - this._currentTime) * 40.0 / this._tickTime;
        if (note.n === 4 || note.n === 6)
            xPos += 5;
        if (note.n % 3 === 0)
            xPos = this.consts_.canvasWidth_ - xPos - sprite.width;

        spriteFrameName += ((imageIndex + 1) + ".png");
        sprite.setSpriteFrame(spriteFrameName);
        sprite.setPosition(xPos, yPos);
        sprite.setVisible(true);
    },

    _updateSpaceNote: function (note) {
        let width = this.consts_.canvasWidth_;
        let xPos = width / 2 - (note.t - this._currentTime) / this._tickTime * 31.0 / 2;
        let yPos = this.consts_.spaceMarginBottom_;
        note._lSpace.setPosition(xPos, yPos);
        note._rSpace.setPosition(width - xPos, yPos);
    }
});