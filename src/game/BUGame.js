let BUGame = cc.Class.extend({
    ctor: function (view) {
        this._view = view;

        this.consts_ = GLOBAL_CONSTS;

        this._spaceScore = 2000;
        this._noteScores = [480, 240, 120, 60, 0];
        this._yellowBeatupRatio = 1.2;
        this._blueBeatupRatio = 1.44;

        this.reset();
        this._autoPlay = false;
        this._idle = true;
    },

    update: function (dt) {
        if (this._idle)
            return;

        this._currentTime += (dt * 1000);

        this._checkMiss();
    },

    reset: function (songInfo) {
        this._currentTime = 0;
        this._score = 0;
        this._pgcbm = [0, 0, 0, 0, 0];
        this._combo = 0;
        this._perx = 0;
        if (!songInfo)
            return;

        if (songInfo.hasOwnProperty("ogg") === false ||
            songInfo.hasOwnProperty("singer") === false ||
            songInfo.hasOwnProperty("name") === false ||
            songInfo.hasOwnProperty("slkauthor") === false ||
            songInfo.hasOwnProperty("bpm") === false) {
            // reset to idle
            this._idle = true;
            return;
        }

        this._bpm = songInfo["bpm"];
        this._notes = songInfo["notes"];
        this._tickTime = 1000 * 60.0 / (this._bpm * 4);
        this._convertTickToMs();

        this._idle = false;
        this._view.startGame(songInfo["ogg"], this._notes, this._tickTime);
        this._refreshFirstAvailableNote();
    },

    /**
     * Handle key input
     * @param keyCode
     */
    processKeyInput: function (keyCode) {
        let keyInput = -1;
        if (keyCode === cc.KEY.a) {
            this._autoPlay = !this._autoPlay;
            console.log("Autoplay : " + this._autoPlay);
            return true;
        }
        if (keyCode === cc.KEY.num1 || keyCode === cc.KEY.b) {
            keyInput = 1;
        } else if (keyCode === cc.KEY.num4 || keyCode === cc.KEY.g) {
            keyInput = 4;
        } else if (keyCode === cc.KEY.num7 || keyCode === cc.KEY.t) {
            keyInput = 7;
        } else if (keyCode === cc.KEY.num3 || keyCode === cc.KEY.m) {
            keyInput = 3;
        } else if (keyCode === cc.KEY.num6 || keyCode === cc.KEY.j) {
            keyInput = 6;
        } else if (keyCode === cc.KEY.num9 || keyCode === cc.KEY.u) {
            keyInput = 9;
        } else if (keyCode === cc.KEY.num5 || keyCode === cc.KEY.space || keyCode === cc.KEY.h) {
            keyInput = 5;
        }

        if (keyInput > 0) {
            this._view.showKeyInput(keyInput);
            this._processNoteResult(keyInput);
            return true;
        }
        return false;
    },

    _convertTickToMs: function () {
        for (let i = 0; i < this._notes.length; i++) {
            this._notes[i].t = this._notes[i].t * this._tickTime;
        }
    },

    _checkMiss: function () {
        let maxNote = Math.min(this._firstAvailNote + this.consts_.numNotes_, this._notes.length);
        if (this._firstAvailNote >= 0) {
            for (let i = this._firstAvailNote; i < maxNote; i++) {
                if (this._autoPlay) {
                    // autoplay place here
                    // auto play
                    if (this._notes[i].t - 5 <= this._currentTime) {
                        this._processNoteResult(this._notes[i].n);
                        break;
                    }
                } else {
                    if (this._currentTime > this._notes[i].t + this._tickTime * 2) {
                        this._notes[i]._pressed = true;
                        this._view.setKeyResult(4, this._notes[i].n, i);
                        this._updateScore(this._notes[i].n, 4);
                    }
                }
            }
        }

        // recalculate first available note
        this._refreshFirstAvailableNote();

    },

    _processNoteResult: function (keyCode) {
        let noteResult = -1;
        for (let i = this._firstAvailNote; i < this._firstAvailNote + 5 && i < this._notes.length; i++) {
            let note = this._notes[i];
            let noteKey = note.n;
            let keyTime = this._currentTime - note.t;

            if (noteKey === keyCode) {
                noteResult = this._getKeyResult(keyTime);
                if (noteResult >= 0) {
                    this._view.setKeyResult(noteResult, noteKey, i);
                    this._updateScore(noteKey, noteResult);
                    note._pressed = true;

                    // recalculate first available note
                    this._refreshFirstAvailableNote();
                    break;
                }
            }
        }
    },

    _getKeyResult: function (diff) {
        let ratio = 4;
        let frameTime = this._tickTime * ratio;
        if (diff > 0.8 * frameTime || diff < -frameTime)
            return -1; // don't process
        diff = Math.abs(diff);
        if (diff <= 0.05 * frameTime) return 0; // p
        if (diff <= 0.15 * frameTime) return 1; // g
        if (diff <= 0.27 * frameTime) return 2; // c
        if (diff <= 0.4 * frameTime) return 3; // b
        return 4; // m
    },

    /**
     * Update score by key result
     * @param keyNote key 1|3|4|5|6|7|9
     * @param result p/g/c/b/m 0|1|2|3|4
     */
    _updateScore: function (keyNote, result) {
        let noteScore = 0;
        if (keyNote === 5) {
            if (0 <= result && result < 4) {
                noteScore = this._spaceScore;
            }
        } else {
            if (result >= 0) {
                noteScore = this._noteScores[result];
            }
        }

        // ratios with BEATUP
        if (this._combo >= 400) noteScore *= this._blueBeatupRatio;
        else if (this._combo >= 100) noteScore *= this._yellowBeatupRatio;
        this._score += noteScore;

        // result : pgcbm
        this._pgcbm[result]++;

        // update combo
        //var prevCombo = _this.combo_;
        if (0 <= result && result < 4) this._combo++;
        else this._combo = 0;

        // update perx
        if (result === 0) {		// still per?
            this._perx++;
        } else this._perx = 0;

        this._view.updateCombo(this._combo);
        this._view.updateScore(this._score, this._pgcbm, this._perx);
    },

    _refreshFirstAvailableNote: function () {
        this._firstAvailNote = -1;
        for (let i = 0; i < this._notes.length; i++) {
            if (!this._notes[i]._pressed) {
                this._firstAvailNote = i;
                break;
            }
        }
    }
});