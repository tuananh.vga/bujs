/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

let BUScene = cc.Scene.extend({
    ctor: function () {
        this._super();
        this.consts_ = GLOBAL_CONSTS;

        this.initViews();
        this._game = new BUGame(this);
        this._sounds = {
            space: "sound/space.wav",
            perfect: "sound/perfect.wav",
            miss: "sound/miss.wav",
            normal: "sound/normal.wav"
        }
        this._audioEngine = cc.audioEngine;
    },

    initViews: function () {
        this.initSpaceFrame();
        this.initLanes();
        this.initNoteFrame();
        this.initScoreLabel();
        this.initKeyResultFrame();

        let thiz = this;
        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed: function () {
                BUScene.prototype.onKeyDown.apply(thiz, arguments);
            },
            onKeyReleased: function () {

            }
        },this);
    },

    initLanes: function () {
        this.laneNode = new cc.Node();
        this.addChild(this.laneNode);

        let Sprite00 = cc.Sprite.extend({
            ctor: function () {
                cc.Sprite.prototype.ctor.apply(this, arguments);
                this.setAnchorPoint(0, 0);
            }
        });

        let laneL = new Sprite00("#laneL.png");
        laneL.setPosition(this.consts_.tableWidth_
            - this.consts_.tableWidthTrans_
            - this.consts_.chanceDist_, this.consts_.laneYStart_);
        this.laneNode.addChild(laneL);

        let laneR = new Sprite00("#laneR.png");
        laneR.setPosition(this.width - this.consts_.tableWidth_
            + this.consts_.tableWidthTrans_ - this.consts_.laneWidth_
            + this.consts_.chanceDist_, this.consts_.laneYStart_);
        this.laneNode.addChild(laneR);

        let tableL = new Sprite00("#tableL.png");
        tableL.setPosition(0, this.consts_.laneYStart_);
        this.laneNode.addChild(tableL,1);

        let tableR = new Sprite00("#tableR.png");
        tableR.setPosition(this.width - tableR.width, this.consts_.laneYStart_);
        this.laneNode.addChild(tableR,1);

        let landingL = new Sprite00("#landingL.png");
        landingL.setPosition(laneL.x + this.consts_.laneWidth_, this.consts_.laneYStart_);
        this.laneNode.addChild(landingL);

        let landingR = new Sprite00("#landingR.png");
        landingR.setPosition(this.width - this.consts_.tableWidth_
            + this.consts_.tableWidthTrans_ - this.consts_.laneWidth_
            - landingR.width + this.consts_.chanceDist_,
            this.consts_.laneYStart_);
        this.laneNode.addChild(landingR);
    },

    initSpaceFrame: function () {
        let spaceFrame = new SpaceFrame();
        spaceFrame.setPosition(this.width / 2, this.consts_.spaceMarginBottom_);
        this.addChild(spaceFrame);
        this.spaceFrame = spaceFrame;
    },

    initNoteFrame: function (){
        let noteFrame = new NoteFrame();
        this.laneNode.addChild(noteFrame);
        this.noteFrame = noteFrame;
    },

    initScoreLabel: function(){
        let label = new cc.LabelTTF("","Arial",12);
        label.setAnchorPoint(cc.p(0.5,1));
        label.setPosition(this.width/2, 230);
        label.setDimensions(200,0);
        this.addChild(label);
        this.scoreLabel = label;
    },

    initKeyResultFrame: function () {
        let keyResultFrames = new KeyResultFrame();
        this.addChild(keyResultFrames);
        this.keyResultFrames = keyResultFrames;
    },

    setSong: function (songInfo) {
        let thiz = this;
        let song = new Song(songInfo);
        song.setOnReadyFunction(function (songEntry) {
            thiz._game.reset(songEntry);
        })
    },

    /**
     * Handle start game request from game obj <br/>
     * @function
     * @param {String} music Song's name
     * @param {Notes} notes Song's notes
     * @param {number} tickTime Song's tick time
     */
    startGame: function (music, notes,tickTime){
        this.noteFrame.start(notes,tickTime);
        this._audioEngine.playMusic("/music/" + music,false);
    },

    /**
     * Update key result from game, remove note index from display
     * @param result {number} p/g/c/b/m 0|1|2|3|4
     * @param noteKey {number} key
     * @param index {number} note index
     */
    setKeyResult: function (result,noteKey,index){
        this.keyResultFrames.setKeyResult(result,noteKey);
        this.noteFrame.removeNote(index);
        if (noteKey === 5 && result !== 4) this._audioEngine.playEffect(this._sounds.space,false);
        else if (result === 0) this._audioEngine.playEffect(this._sounds.perfect,false);  // arrow per?
        else if (result === 4) this._audioEngine.playEffect(this._sounds.miss,false);     // arrow miss?
        else this._audioEngine.playEffect(this._sounds.normal,false);                        // arrow normal
    },

    /**
     * Update combo from game obj
     * @param combo
     */
    updateCombo: function(combo){
        this.spaceFrame.setCombo(combo);
        this._combo = combo;
    },

    /**
     * Update score info from game obj
     * @param score
     * @param pgcbm Perfect/Great/Cool/Bad/Miss
     * @param perx Consecutive perfect
     */
    updateScore: function (score,pgcbm,perx){
        perx--;
        if (perx < 0)
            perx = 0;

        let string = "X/P/G/C/B/M : ";
        string += perx;
        for (let i = 0;i<pgcbm.length;i++){
            string += ("/" + pgcbm[i]);
        }

        string += "\nScore : ";
        string += Math.floor(score);
        string += "\nCombo : ";
        string += this._combo;
        this.scoreLabel.setString(string);
    },

    /**
     * Visualize key input
     * @param keyCode
     */
    showKeyInput: function (keyCode){
        this.noteFrame.showKeyInput(keyCode);
    },

    onEnter: function () {
        this._super();
        this.scheduleUpdate();
    },

    onExit: function () {
        this._super();
        this.unscheduleUpdate();
    },

    onKeyDown: function (keyCode) {
        if (this._game.processKeyInput(keyCode))
            return;
        console.log(keyCode);
    },

    update: function (dt) {
        this._game.update(dt);
    }
});